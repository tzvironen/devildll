#include "good_dll.h"
#include <iostream>

int good()
{
	std::cout << "good function called!" << std::endl;
	return 0;
}


// Template of DllMain to be used in the next dlls.
//BOOL WINAPI DllMain(
//    _In_ HINSTANCE hinstDLL,
//    _In_ DWORD     fdwReason,
//    _In_ LPVOID    lpvReserved
//    )
//{
//    switch (fdwReason)
//    {
//    case DLL_PROCESS_ATTACH:
//        std::cout << "DLL_PROCESS_ATTACH" << std::endl;
//        break;
//    case DLL_PROCESS_DETACH:
//        std::cout << "DLL_PROCESS_DETACH" << std::endl;
//        break;
//    case DLL_THREAD_ATTACH:
//        std::cout << "DLL_THREAD_ATTACH" << std::endl;
//        break;
//    case DLL_THREAD_DETACH:
//        std::cout << "DLL_THREAD_DETACH" << std::endl;
//    break;    default:
//        break;
//    }
//    return TRUE;
//}