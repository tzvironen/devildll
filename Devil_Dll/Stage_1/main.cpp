#include <iostream>
#include <windows.h>

extern int good();

int main()
{
	HMODULE devil_dll = NULL;
	devil_dll = LoadLibraryA("devil_dll_stage_1.dll");
	if (NULL == devil_dll)
	{
		std::cout << "failed to load devil_dll_stage_1!" << std::endl;
		return 1;
	}

	good();
	std::cout << "main proc id: " << GetCurrentM << std::endl;

    HMODULE curr_proc_addrss = GetModuleHandle(NULL);
    PIMAGE_DOS_HEADER dos_header = static_cast<PIMAGE_DOS_HEADER>((void*)curr_proc_addrss);
    PIMAGE_NT_HEADERS nt_header = static_cast<PIMAGE_NT_HEADERS>((void*)(dos_header->e_lfanew+dos_header));
    //PIMAGE_FILE_HEADER file_header = static_cast<PIMAGE_FILE_HEADER>((void*)nt_header->FileHeader);
    IMAGE_OPTIONAL_HEADER optional_header = static_cast<IMAGE_OPTIONAL_HEADER>(nt_header->OptionalHeader);

    if (NULL == curr_proc_addrss)
    {
        std::cerr << "failed to get proccess handle!" << std::endl;
    }

	return 1;
}